import { useCallback } from "react";
import { API_HOST } from "../utils/constant";
import { EventSourcePolyfill } from "event-source-polyfill";
import { getTokenApi } from "./auth";

export function addEmotionApi(emotion, emotionType) {
  const url = `${API_HOST}/api/v1/emotions`;
  const data = {
    emotion: emotion,
    type: emotionType,
  };

  const params = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getTokenApi()}`,
    },
    body: JSON.stringify(data),
  };

  return fetch(url, params).then((response) => {
    return response
      .text()
      .then((body) => {
        if (response.status >= 200 && response.status < 300) {
          let newEmotion = JSON.stringify(convertDataToJSON(body));
          //console.log(newEmotion);
          let result = `{ "code": ${response.status}, "message": "Emoción enviada!", "newEmotion":${newEmotion}}`;
          //console.log(result);
          return JSON.parse(result);
        }
        return { code: 500, message: "Error en el servidor" };
      })
      .catch((err) => {
        return err;
      });
  });
}

//   return await fetch(url, params)
//     .then((response) => {
//       if (response.status >= 200 && response.status < 300) {
//         let data = "";
//         let result = "";

//         response.text().then((s) => {
//           console.log(s);
//           data = s;
//           let newEmotion = convertDataToJSON(data);
//           console.log(newEmotion);
//           result = `{ "code": ${response.status}, "message": "${newEmotion.id}" }`;
//           console.log(result);
//           return result;
//         });
//         console.log("LLEGUE AQUI");
//         return response.text();
//       }
//       return { code: 500, message: "Error en el servidor" };
//     })
//     .then((result) => {
//       console.log(result);
//       return result;
//     })
//     .catch((err) => {
//       return err;
//     });
// }

export async function getUserEmotionApi(idUser, page) {
  const url = `${API_HOST}/api/v1/emotions/users?userId=${idUser}&pagina=${page}`;

  const params = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getTokenApi()}`,
    },
  };

  return await fetch(url, params)
    .then((response) => {
      // var result = response.clone().json();
      // console.log(result);
      // return result;
      //console.log(response.body);
      return response;
    })
    .catch((err) => {
      return err;
    });
}

// const url = `${API_HOST}/api/v1/emotions/users?userId=${params.id}&pagina=${page}`;
// const ssep = new EventSourcePolyfill(url, {
//   headers: {
//     Authorization: `Bearer ${getTokenApi()}`,
//   },
// });
// function getRealtimeData(data) {
//   // process the data here,
//   // then pass it to state to be rendered
//   setEmotions((emotions) => emotions.concat(formatEmotionValue(data)));
// }

// ssep.onmessage = (e) => {
//   //console.log(e);
//   if (e.data != null) {
//     getRealtimeData(JSON.parse(e.data));
//   }
// };
// ssep.onerror = () => {
//   // error log here
//   ssep.close();
//   //console.error("EventSource failed!");
// };
// return () => {
//   ssep.close();
// };

export async function getEmotionsApi(page) {
  const url = `${API_HOST}/api/v1/emotions?pagina=${page}`;

  const params = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getTokenApi()}`,
    },
  };

  return await fetch(url, params)
    .then((response) => {
      var result = response.clone().json();
      console.log(result);
      return response;
    })
    .catch((err) => {
      return err;
    });
}

function convertDataToJSON(data) {
  let result = data.replace(/data:/g, "");
  result = result.replace("emotionType", "type");
  const emotion = JSON.parse(result);
  return emotion;
}
