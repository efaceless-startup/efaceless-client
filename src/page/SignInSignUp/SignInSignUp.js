import React, { useState } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faGrinWink,
  faHandSparkles,
  faHeart,
} from "@fortawesome/free-solid-svg-icons";
import BasicModal from "../../components/Modal/BasicModal/BasicModal";
import SignUpForm from "../../components/SignUpForm/SignUpForm";
import SignInForm from "../../components/SignInForm";
import LogoEfaceless from "../../assets/png/logo-efaceless.png";
import CoverPageEfaceless from "../../assets/png/fondoEfaceless.jpg";
import CoverPageEfacelessRight from "../../assets/png/fondoEfacelessRight.jpg";

import "./SignInSignUp.scss";

export default function SignInSignUp(props) {
  const { setRefreshCheckLogin } = props;
  const [showModal, setShowModal] = useState(false);
  const [contentModal, setContentModal] = useState(null);

  const openModal = (content) => {
    setShowModal(true);
    setContentModal(content);
  };

  return (
    <>
      <Container className="signin-signup" fluid>
        <Row>
          <LeftComponent />
          <RightComponent
            openModal={openModal}
            setShowModal={setShowModal}
            setRefreshCheckLogin={setRefreshCheckLogin}
          />
        </Row>
      </Container>
      <BasicModal show={showModal} setShow={setShowModal}>
        {contentModal}
      </BasicModal>
    </>
  );
}

function LeftComponent() {
  return (
    <Col className="signin-signup__left" xs={6}>
      <img src={CoverPageEfaceless} alt="Efaceless" />
      <div>
        <h2>
          <FontAwesomeIcon icon={faGrinWink} />
          No es tu rostro, son tus emociones!
        </h2>
        <h2>
          <FontAwesomeIcon icon={faHeart} />
          Tu emoción puede emocionar a todo el mundo.
        </h2>
        <h2>
          <FontAwesomeIcon icon={faHandSparkles} />
          Interactua con todas las emociones.
        </h2>
      </div>
    </Col>
  );
}

function RightComponent(props) {
  const { openModal, setShowModal, setRefreshCheckLogin } = props;
  return (
    <Col className="signin-signup__right" xs={6}>
      <img src={CoverPageEfacelessRight} alt="Efaceless" />
      <div>
        <img src={LogoEfaceless} alt="Efaceless" />
        <h2>Emociona al mundo en este momento</h2>
        <h3>Nunca fue tan fácil expresar tus emociones</h3>
        <Button
          variant="primary"
          onClick={() =>
            openModal(<SignUpForm setShowModal={setShowModal}></SignUpForm>)
          }
        >
          Regístrate
        </Button>
        <Button
          variant="outline-primary"
          onClick={() =>
            openModal(
              <SignInForm setRefreshCheckLogin={setRefreshCheckLogin} />
            )
          }
        >
          Iniciar sesión
        </Button>
      </div>
    </Col>
  );
}
