import React, { useState, useEffect } from "react";
import { EventSourcePolyfill } from "event-source-polyfill";
import { API_HOST } from "../../utils/constant";
import { Button, Spinner } from "react-bootstrap";
import { concat } from "lodash";
import BasicLayout from "../../layout/BasicLayout/BasicLayout";
import ListEmotions from "../../components/ListEmotions";
import { getTokenApi } from "../../api/auth";
import { getEmotionsApi } from "../../api/emotion";

import "./Home.scss";

export default function Home(props) {
  const { match, setRefreshCheckLogin, newEmotion, setNewEmotion } = props;
  const [emotions, setEmotions] = useState([]);
  const [page, setPage] = useState(0);
  const [loadingEmotions, setLoadingEmotions] = useState(false);
  const { params } = props;

  useEffect(() => {
    const url = `${API_HOST}/api/v1/emotions?page=${page}`;
    const ssep = new EventSourcePolyfill(url, {
      headers: {
        Authorization: `Bearer ${getTokenApi()}`,
      },
    });
    function getRealtimeData(data) {
      // process the data here,
      // then pass it to state to be rendered
      //console.log(data);
      setEmotions((emotions) => emotions.concat(formatEmotionValue(data)));
    }

    ssep.onmessage = (e) => {
      //console.log(e);
      if (e.data != null) {
        getRealtimeData(JSON.parse(e.data));
      }
    };
    ssep.onerror = () => {
      // error log here
      ssep.close();
      //console.error("EventSource failed!");
    };
    return () => {
      ssep.close();
    };
  }, [params]);

  useEffect(() => {
    if (emotions !== null && newEmotion !== null) {
      setEmotions((old) => [newEmotion, ...old]);
      setNewEmotion(null);
    }
  }, [newEmotion]);

  const moreData = () => {
    const pageTemp = page + 1;
    setLoadingEmotions(true);
    getEmotionsApi(pageTemp).then((response) => {
      if (!response) {
        setLoadingEmotions(0);
      } else {
        //console.log(response);
        //setTweets([...tweets, ...response]);
        setPage(pageTemp);
        setLoadingEmotions(false);
      }
    });
  };

  return (
    <BasicLayout
      className="home"
      setRefreshCheckLogin={setRefreshCheckLogin}
      newEmotion={newEmotion}
      setNewEmotion={setNewEmotion}
    >
      <div className="home__title">
        <h2>Inicio</h2>
      </div>
      <div className="home__emotions">
        {emotions && <ListEmotions emotions={emotions} />}
        <Button onClick={moreData}>
          {!loadingEmotions ? (
            loadingEmotions !== 0 && "Obtener más Emociones"
          ) : (
            <Spinner
              as="span"
              animation="grow"
              size="sm"
              role="status"
              arian-hidden="true"
            />
          )}
        </Button>
      </div>
    </BasicLayout>
  );
}

function formatEmotionValue(emotion) {
  const emotionsTemp = [];
  emotionsTemp.push({
    id: emotion.id || "",
    emotion: emotion.emotion || "",
    type: emotion.emotionType || "",
    creationDateTime: emotion.creationDateTime || "",
  });

  return emotionsTemp;
}
