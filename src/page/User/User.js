import React, { useState, useEffect, useCallback } from "react";
import { EventSourcePolyfill } from "event-source-polyfill";
import { Button, Spinner } from "react-bootstrap";
import { withRouter } from "react-router";
import { toast } from "react-toastify";
import { concat } from "lodash";
import { API_HOST } from "../../utils/constant";
import useAuth from "../../hooks/useAuth";
import BasicLayout from "../../layout/BasicLayout";
//import BannerAvatar from "../../components/User/BannerAvatar/BannerAvatar";
//import InfoUser from "../../components/User/InfoUser/InfoUser";
import ListEmotions from "../../components/ListEmotions";
//import { getUserApi } from "../../api/user";
import { getTokenApi } from "../../api/auth";
import { getUserEmotionApi } from "../../api/emotion";

import "./User.scss";

function User(props) {
  const { match, setRefreshCheckLogin, newEmotion, setNewEmotion } = props;
  //console.log("USER");
  //console.log(newEmotion);
  const [user, setUser] = useState(null);
  const [emotions, setEmotions] = useState([]);
  const [page, setPage] = useState(1);
  const [loadingEmotions, setLoadingEmotions] = useState(false);
  const { params } = match;
  const loggedUser = useAuth();

  // useEffect(() => {
  //   getUserApi(params.id)
  //     .then((response) => {
  //       setUser(response);
  //       if (!response) toast.error("El usuario que has visitado no existe");
  //     })
  //     .catch(() => {
  //       toast.error("El usuario que has visitado no existe");
  //     });
  // }, [params]);

  useEffect(() => {
    const url = `${API_HOST}/api/v1/emotions/users?userId=${params.id}&pagina=${page}`;
    const ssep = new EventSourcePolyfill(url, {
      headers: {
        Authorization: `Bearer ${getTokenApi()}`,
      },
    });
    function getRealtimeData(data) {
      // process the data here,
      // then pass it to state to be rendered
      setEmotions((emotions) => emotions.concat(formatEmotionValue(data)));
    }

    ssep.onmessage = (e) => {
      //console.log(e);
      if (e.data != null) {
        getRealtimeData(JSON.parse(e.data));
      }
    };
    ssep.onerror = () => {
      // error log here
      ssep.close();
      //console.error("EventSource failed!");
    };
    return () => {
      ssep.close();
    };
  }, [params]);

  useEffect(() => {
    if (emotions !== null && newEmotion !== null) {
      setEmotions((old) => [newEmotion, ...old]);
      setNewEmotion(null);
    }
  }, [newEmotion]);

  const moreData = () => {
    const pageTemp = page + 1;
    setLoadingEmotions(true);
    getUserEmotionApi(params.id, pageTemp).then((response) => {
      if (!response) {
        setLoadingEmotions(0);
      } else {
        //console.log(response);
        //setTweets([...tweets, ...response]);
        setPage(pageTemp);
        setLoadingEmotions(false);
      }
    });
  };

  return (
    <BasicLayout
      className="user"
      setRefreshCheckLogin={setRefreshCheckLogin}
      newEmotion={newEmotion}
      setNewEmotion={setNewEmotion}
    >
      <div className="user__emotions">
        <h2>Emociones</h2>
        {emotions && <ListEmotions emotions={emotions}></ListEmotions>}
        <Button onClick={moreData}>
          {!loadingEmotions ? (
            loadingEmotions !== 0 && "Obtener más Emociones"
          ) : (
            <Spinner
              as="span"
              animation="grow"
              size="sm"
              role="status"
              arian-hidden="true"
            />
          )}
        </Button>
      </div>
    </BasicLayout>
  );
}

function formatEmotionValue(emotion) {
  const emotionsTemp = [];
  emotionsTemp.push({
    id: emotion.id || "",
    emotion: emotion.emotion || "",
    type: emotion.emotionType || "",
    creationDateTime: emotion.creationDateTime || "",
  });

  return emotionsTemp;
}

export default withRouter(User);
