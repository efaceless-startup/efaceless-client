import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome, faUser, faPowerOff } from "@fortawesome/free-solid-svg-icons";
import EmotionModal from "../Modal/EmotionModal/EmotionModal";
import { logoutApi } from "../../api/auth";
import useAuth from "../../hooks/useAuth";
import Logo from "../../assets/png/logo-efaceless.png";

import "./LeftMenu.scss";

export default function LeftMenu(props) {
  const { setRefreshCheckLogin, newEmotion, setNewEmotion } = props;
  const [showModal, setShowModal] = useState(false);
  const user = useAuth();

  const logout = () => {
    logoutApi();
    setRefreshCheckLogin(true);
  };

  return (
    <div className="left-menu">
      <img className="logo" src={Logo} alt="Efaceless" />
      <Link to="/">
        <FontAwesomeIcon icon={faHome} />
        Inicio
      </Link>
      <Link to={`/${user?.jti}`}>
        <FontAwesomeIcon icon={faUser} />
        Mi emociones
      </Link>
      <Link to="" onClick={logout}>
        <FontAwesomeIcon icon={faPowerOff} />
        Cerrar sessión
      </Link>
      <Button onClick={() => setShowModal(true)}>E!</Button>
      <EmotionModal
        show={showModal}
        setShow={setShowModal}
        newEmotion={newEmotion}
        setNewEmotion={setNewEmotion}
      ></EmotionModal>
    </div>
  );
}
