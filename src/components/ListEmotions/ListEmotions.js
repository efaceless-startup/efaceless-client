import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSmileWink,
  faAngry,
  faFrown,
  faFlushed,
  faSadCry,
} from "@fortawesome/free-solid-svg-icons";
import { map } from "lodash";
import moment from "moment";
import localization from "moment/locale/es";
import AvatarNotFound from "../../assets/png/avatar-no-found.png";
import { getUserApi } from "../../api/user";
import { API_HOST } from "../../utils/constant";
import { replaceURLWithHTMLLinks } from "../../utils/functions";

import "./ListEmotions.scss";

export default function ListEmotions(props) {
  const { emotions } = props;

  return (
    <div className="list-tweets">
      {map(emotions, (emotion, index) => (
        <Emotion key={index} emotion={emotion} />
      ))}
    </div>
  );
}

function Emotion(props) {
  const { emotion } = props;
  //const [userInfo, setUserInfo] = useState(null);
  //const [avatarUrl, setAvatarUrl] = useState(null);

  // useEffect(() => {
  //   getUserApi(tweet.userid).then((response) => {
  //     console.log(response);
  //     setUserInfo(response);
  //     setAvatarUrl(
  //       response?.avatar
  //         ? `${API_HOST}/obtenerAvatar?id=${response.id}`
  //         : AvatarNotFound
  //     );
  //   });
  // }, [tweet]);

  return (
    <div className="tweet">
      <EmotionIcon emotion={emotion} />
      <div>
        <div className="name">
          <div className={determinateEmotionType(emotion)}>{emotion?.type}</div>
          <span>
            {moment(emotion.creationDateTime)
              .locale("es", localization)
              .calendar()}
          </span>
        </div>
        <div
          dangerouslySetInnerHTML={{
            __html: replaceURLWithHTMLLinks(emotion.emotion),
          }}
        />
      </div>
    </div>
  );

  function determinateEmotionType(emotion) {
    return emotion.type !== "" ? emotion.type.toLowerCase() : "";
  }
}

function EmotionIcon(props) {
  const { emotion } = props;
  const typeIcon = determinateEmotionIcon(emotion.type);
  const colorTypeIcon = determinateEmotionIconColor(emotion.type);

  return <FontAwesomeIcon icon={typeIcon} color={colorTypeIcon} />;

  function determinateEmotionIcon(type) {
    let icon = faSmileWink;
    // eslint-disable-next-line default-case
    switch (type) {
      case "JOY":
        icon = faSmileWink;
        break;
      case "ANGER":
        icon = faAngry;
        break;
      case "SADNESS":
        icon = faSadCry;
        break;
      case "FEAR":
        icon = faFlushed;
        break;
      case "DISGUST":
        icon = faFrown;
        break;
    }
    return icon;
  }

  function determinateEmotionIconColor(type) {
    let color = "yellow";
    // eslint-disable-next-line default-case
    switch (type) {
      case "JOY":
        color = "yellow";
        break;
      case "ANGER":
        color = "red";
        break;
      case "SADNESS":
        color = "blue";
        break;
      case "FEAR":
        color = "blueviolet";
        break;
      case "DISGUST":
        color = "green";
        break;
    }
    return color;
  }
}
