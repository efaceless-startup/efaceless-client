import React, { useState } from "react";
import { Modal, Form, ButtonGroup, Button } from "react-bootstrap";
import classNames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSmileWink,
  faAngry,
  faFrown,
  faFlushed,
  faSadCry,
} from "@fortawesome/free-solid-svg-icons";
import { toast } from "react-toastify";
import { Close } from "../../../utils/Icons";
import { addEmotionApi } from "../../../api/emotion";

import "./EmotionModal.scss";

export default function EmotionModal(props) {
  const { show, setShow, newEmotion, setNewEmotion } = props;
  const [emotion, setEmotion] = useState("");
  const [emotionType, setEmotionType] = useState("");
  const [currentEmotionType, setCurrentEmotionType] = useState("");
  const [colorEmotion, setColorEmotion] = useState("");
  const maxLength = 280;

  const onSubmit = (e) => {
    e.preventDefault();

    if (emotion.length > 0 && emotion.length <= maxLength) {
      addEmotionApi(emotion, emotionType)
        .then((response) => {
          if (response?.code >= 200 && response?.code < 300) {
            toast.success(response.message);
            setShow(false);
            //console.log(response.newEmotion);
            setNewEmotion(response.newEmotion);
            //console.log("EMOTION MODAL");
            //console.log(newEmotion);
            //window.location.reload();
          }
        })
        .catch(() => {
          toast.warning("Error al enviar la emoción, intentelo más tarde");
        });
    }
  };

  const onEmotionTypeClicked = (emotionType, color) => {
    setCurrentEmotionType(emotionType);
    setColorEmotion(color);
  };

  return (
    <Modal
      className="emotion-modal"
      show={show}
      onHide={() => setShow(false)}
      centered
      size="lg"
    >
      <Modal.Header>
        <Modal.Title>
          <Close onClick={() => setShow(false)} />
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={onSubmit}>
          <Form.Control
            as="textarea"
            rows="6"
            placeholder="¿Que te está emocionando?"
            onChange={(e) => setEmotion(e.target.value)}
          />
          <ButtonGroup className="type">
            <Button onClick={() => setEmotionType("joy")}>
              <FontAwesomeIcon
                icon={faSmileWink}
                color={
                  currentEmotionType === "joy" ? colorEmotion : "lightyellow"
                }
                onClick={() => onEmotionTypeClicked("joy", "yellow")}
              />
            </Button>
            <Button onClick={() => setEmotionType("anger")}>
              <FontAwesomeIcon
                icon={faAngry}
                color={
                  currentEmotionType === "anger" ? colorEmotion : "lightcoral"
                }
                onClick={() => onEmotionTypeClicked("anger", "red")}
              />
            </Button>
            <Button onClick={() => setEmotionType("disgust")}>
              <FontAwesomeIcon
                icon={faFrown}
                color={
                  currentEmotionType === "disgust" ? colorEmotion : "lightgreen"
                }
                onClick={() => onEmotionTypeClicked("disgust", "green")}
              />
            </Button>
            <Button onClick={() => setEmotionType("fear")}>
              <FontAwesomeIcon
                icon={faFlushed}
                color={
                  currentEmotionType === "fear" ? colorEmotion : "mediumpurple"
                }
                onClick={() => onEmotionTypeClicked("fear", "purple")}
              />
            </Button>
            <Button onClick={() => setEmotionType("sadness")}>
              <FontAwesomeIcon
                icon={faSadCry}
                color={
                  currentEmotionType === "sadness" ? colorEmotion : "lightblue"
                }
                onClick={() => onEmotionTypeClicked("sadness", "blue")}
              />
            </Button>
          </ButtonGroup>
          <span
            className={classNames("count", {
              error: emotion.length > maxLength,
            })}
          >
            {emotion.length}
          </span>
          <Button
            type="submit"
            disabled={emotion.length > maxLength || emotion.length < 1}
          >
            Emocioná al Mundo!
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
}
